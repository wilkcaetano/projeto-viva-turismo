<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


$this->group(['middleware' => 'auth'], function () {
    Route::get('/clientes/remove/{id}', 'ClienteController@remove')->name('clientes.remove');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('clientes', 'ClienteController@register')->name('register');
    Route::resource('clientes', 'ClienteController');
	$this->get('clientes/pdf/{id}', 'ClienteController@download')->name('pdf');
	Route::post('documentos/{id}', 'ClienteController@documentos')->name('documentos');
	 Route::get('documentos/{id}', 'ClienteController@documentosdelete')->name('documentosdelete');
});
//Route::get('cliente', 'ClienteController@index')->name('cliente.cadastrar');
//Route::get('cliente', 'ClienteController@show')->name('show');
//Route::post('cliente', 'ClienteController@store')->name('cliente.cadastro');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
