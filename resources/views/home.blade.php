<?php
	function limit($iterable, $limit) {
			foreach ($iterable as $key => $value) {
				if (!$limit--) break;
				yield $key => $value;
			}
		}
?>
@extends('adminlte::page')


@section('title', 'Sistema Viva Turismo')

@section('content_header')
   <h1 style="text-align:center;">Seja Muito Bem Vindo oa Sistema de cadastro de Usuários da Empresa Viva Turismo</h1>
@stop

@section('content')
    <div class="container-fluid">
        <!--<div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            </div>
            
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Likes</span>
                    <span class="info-box-number">41,410</span>
                </div>
                
            </div>
           
        </div>
      
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sales</span>
                    <span class="info-box-number">760</span>
                </div>
               
            </div>
           
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">New Members</span>
                    <span class="info-box-number">2,000</span>
                </div>
                
            </div>
         
        </div>-->
        <!-- /.col -->

    <!-- /.row -->
	
    <div class="col-md-12" style="padding: 0px;">
        @foreach( limit($clientes, 6)  as $index => $cliente)
        <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                    <h3 class="widget-user-username">{{ $cliente->nome }}</h3>
                    <h5 class="widget-user-desc">
					
					@if(empty($juridica[$index]['cnpj']))
						{{ $pessoa[$index]['profissao'] }}
					@else
					{{ "Empresa" }}
						
					@endif
                    </h5>
                </div>
                <div class="widget-user-image">
                    @if(!empty($cliente->image))
                    <img class="img-circle" src="{{ url('uploads/avatar/'.$cliente->id.'/'.$cliente->image) }}" alt="" style="width: 100px;height: 100px;">
                    @else
                        @if(empty($pessoa[$index]->cpf))
                            <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="uploads/avatar/empresa.png" alt="" style="width: 100px;height: 100px;">
                         @elseif($pessoa[$index]->sexo == "Feminino")
                            <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="uploads/avatar/avatarf.png" alt="" style="width: 100px;height: 100px;">
                            @elseif($pessoa[$index]->sexo == 'Masculino')
                            <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="uploads/avatar/avatar.jpg" alt="" style="width: 100px;height: 100px;">
                        @endif
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
							<h5 class="description-header">
                                    @if(!empty($pessoa[$index]->cpf))
                                        {{ "CPF" }}
                                    @else
                                        {{ "CNPJ" }}
                                    @endif
                                </h5>
                                <span class="description-text">
                                    @if(!empty($pessoa[$index]->cpf))
                                        {{ $pessoa[$index]->cpf }}
                                    @else
                                        {{ $juridica[$index]->cnpj }}
                                    @endif
                                </span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 border-right" style="text-align: center;">
                            <a href="{{ route('clientes.show', $cliente->id) }}">
                                <i class="fa fa-eye" style="margin-top: 20px;font-size: 18px;"></i>
                            </a>
                            <!-- /.description-block -->
                        </div>
                        <div class="col-sm-4 border-right" style="text-align: center;">
                            <a href="{{ route('clientes.edit', $cliente->id) }}">
                                <i class="fa fa-pencil" style="margin-top: 20px;font-size: 18px;"></i>
                            </a>
                            <!-- /.description-block -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endforeach
    </div>
    </div>
@stop