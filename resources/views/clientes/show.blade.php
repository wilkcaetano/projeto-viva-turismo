@extends('adminlte::page')

@section('title', 'Sistema Viva Turismo')
<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
@section('content_header')

@stop

@section('content')
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Incluir Documentação para {{ $clientes->nome }}</h4>
                </div>
                <form action="{{ route('documentos', $clientes->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">

                            <div class="moldura-foto" style="margin: 0 auto; width: 150px; min-height:100px; background: #edecec; overflow: hidden;">
                                <img src="{{ asset('img/img.jpg') }}" id="uploadPreview" alt="" width="100%">
                            </div>
                        <div class="arquivo" style="margin: 0 auto; width: 50%; margin-top: 10px;">
                            <input type="text" name="nome" placeholder="Nome do Documento" style="width: 100%; border-radius: 5px; margin-bottom: 10px; padding-left: 5px;">
                            <input type="file" name="image" id="uploadImage" onchange="PreviewImage();" style="width: 100%;border-radius: 5px;border: 1px solid gray;">
                            <input type="hidden" name="id_cliente" value="{{ $clientes->id }}">
                        </div>

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Salvar Documento</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

    </div>
    <!-- /.modal -->
    @if(Session::has('message'))
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Incluir Documentação para {{ $clientes->nome }}</h4>
                </div>

                    <div class="modal-body">
                        <div id="msg" class="alert alert-success" style="text-align: center;">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                    </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

    </div>
    @endif
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        @for($i = 0; $i <= count(Request::segments()); $i++)
            <li>
                <a href="">{{Request::segment($i)}}</a>
                @if($i < count(Request::segments()) & $i > 0)
                    {!!'<i class="fa fa-angle-right"></i>'!!}
                @endif
            </li>
        @endfor
    </ul>
    <div class="container-fluid">
        <!-- Main content -->
        <div class="col-md-3">

            <div class="row">
                <div class="col-md-12">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            @if(!empty($clientes->image))

                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/".$clientes->id."/".$clientes->image) }}" alt="" style="width: 100px;height: 100px;">

                            @elseif(!empty($pessoa->cpf) and $pessoa->sexo == "Masculino")
                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/avatar.jpg") }}" alt="" style="width: 100px;height: 100px;">
                            @elseif(!empty($pessoa->cpf) and $pessoa->sexo == "Feminino")
                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/avatarf.png") }}" alt="" style="width: 100px;height: 100px;">
                            @else
                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/empresa.png") }}" alt="" style="width: 100px;height: 100px;">
                            @endif
                            <h3 class="profile-username text-center">{{ $clientes['nome'] }}</h3>

                            <p class="text-muted text-center">Profissão: {{ $pessoa['profissao'] }}</p>

                            <ul class="list-group list-group-unbordered">
                                @if(!empty($pessoa['cpf']))
                                <li class="list-group-item">
                                    <b>Cpf/Cnpj</b> <a class="pull-right">{{ $pessoa['cpf'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Nacionalidade</b> <a class="pull-right">{{ $pessoa['nacionalidade'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Estado Civil</b> <a class="pull-right">{{ $pessoa['estadoCivil'] }}</a>
                                </li>
                                    @else
                                <li class="list-group-item">
                                    <b>Cpf/Cnpj</b> <a class="pull-right">{{ $juridicap->cnpj }}</a>
                                </li>
                                    @endif
									<li class="list-group-item">
                                        <a href="{{ route('pdf', $clientes->id) }}">
                                            <button type="button" class="btn btn-danger">
                                                Download PDF
                                            </button>
                                        </a>
                                    </li>
                                <li class="list-group-item">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                                        Incluir Documentação
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- About Me Box -->

                    <!-- /.box -->
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <!-- Post -->
                        <div class="post">
                            <div class="user-block">
							@if(!empty($clientes->image))

                                <img class="img-circle img-bordered-sm" id="uploadPreview" src="{{ url("uploads/avatar/".$clientes->id."/".$clientes->image) }}" alt="">

                            @elseif(!empty($pessoa->cpf and $pessoa->sexo == "Masculino"))
                                <img class="img-circle img-bordered-sm" id="uploadPreview" src="{{ url("uploads/avatar/avatar.jpg") }}" alt="">
                            @elseif(!empty($pessoa->cpf and $pessoa->sexo == "Feminino"))
                                    <img class="img-circle img-bordered-sm" id="uploadPreview" src="{{ url("uploads/avatar/avatarf.png") }}" alt="">
                                @else
                                <img class="img-circle img-bordered-sm" id="uploadPreview" src="{{ url("uploads/avatar/empresa.png") }}" alt="">
                            @endif
                                <span class="username">
                          <a href="#">{{ $clientes->nome }}.</a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                                <span class="description">Cliene cadastrado em {{ date( 'd/m/Y  H:m:s' , strtotime($clientes->created_at)) }}</span>
                            </div>
                            <!-- /.user-block -->
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Data de Nascimento: <i style="color: royalblue">{{ date('d/m/Y', strtotime($clientes->dataNasc)) }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Compras: <i style="color: royalblue">{{ $clientes->compras }}</i>
                            </p>
                            <h3 style="text-align: center;">Contatos</h3>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Celular: <i style="color: royalblue">{{ $contato->foneCelular }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Telefone Fixo: <i style="color: royalblue">{{ $contato->foneFixo }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Telefone Para Recado: <i style="color: royalblue">{{ $contato->foneRecado }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Email: <i style="color: royalblue">{{ $contato->email }}</i>
                            </p>
                            <h3 style="text-align: center;">Endereço</h3>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                País: <i style="color: royalblue">{{ $endereco->pais }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Estado: <i style="color: royalblue">{{ $endereco->estado }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Cidade: <i style="color: royalblue">{{ $endereco->cidade }}</i>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($documentos))
            <div class="col-md-12" style="background: white; padding-bottom: 10px;">
                <h4 class="username">Documentação do Cliente</h4>
                <hr>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Imagem</th>
                        <th scope="col">Nome do Documento</th>
                        <th scope="col">Data de Criação</th>
                        <th scope="col">Download</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    @foreach($documentos as $documento)
                        @if($documento->id_cliente == $clientes->id)
                        <tbody>
                        <tr>
                            <td>
                                @if(strrchr($documento->image,'.') == ".pdf" )
                                    <img class="profile-user-img img-responsive img-circle" src="{{ asset('uploads/pdf.ico') }}" alt="" style="width: 50px;">
                                @else
                                <img class="profile-user-img img-responsive img-circle" src="{{ asset('uploads/avatar/'.$clientes->id.'/'.$documento->image) }}" alt="" style="width: 50px;">
                                @endif
                            </td>
                            <td>
                                {{ $documento->nome }}
                            </td>
                            <td>
                                {{ date('d/m/Y', strtotime($documento->created_at)) }}
                            </td>
                            <td>
                                <a href="{{ url('uploads/avatar/'.$clientes->id.'/'.$documento->image) }}"><i class="fa fa-cloud-download" style="margin-left: 25%;"></i></a>
                            </td>
                            <td>
                                <a href="{{ route('documentosdelete', $documento->id) }}"><i class="fa fa-times-circle" style="color: darkred;margin-left: 25%;font-size: 18px;"></i></a>
                            </td>
                        </tr>
                        </tbody>
                       @endif
                    @endforeach
                </table>
            </div>
        @endif
        </div>
    </div>

    <script type="text/javascript">
        function PreviewImage() {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

            oFReader.onload = function (oFREvent) {
                //document.getElementById("uploadPreview").src = oFREvent.target.result;
                document.getElementById("uploadPreview").src = oFREvent.target.result;
            };
        };

    </script>
    @if(Session::has('message'))
        <script>
            $(document).ready(function(){
                $("#exampleModal").modal();
            })
        </script>
    @endif
@stop
