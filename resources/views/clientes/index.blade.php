@extends('adminlte::page')

@section('title', 'Sistema Viva Turismo')
<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
@section('content_header')
@stop

@section('content')
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        @for($i = 0; $i <= count(Request::segments()); $i++)
            <li>
                <a href="">{{Request::segment($i)}}</a>
                @if($i < count(Request::segments()) & $i > 0)
                    {!!'<i class="fa fa-angle-right"></i>'!!}
                @endif
            </li>
        @endfor
    </ul>
 <div class="container-fluid" style="background: white;padding: 10px;">
  <h1>Lista de Todos Clientes Cadastrados</h1>
  <table id="minhaTabela" style="background: white;">
      <thead>
      <tr>
          <th>Foto Perfil</th>
          <th>Nome</th>
          <th>E-mail</th>
          <th>Telefone</th>
          <th>Editar</th>
          <th>Deletar</th>
          <th>Visualizar</th>
      </tr>
      </thead>
      <tbody>
      @foreach($clientes as $cliente)
          <tr>
              <td>
                  @foreach($pessoa as $pessoas)
                      @if($pessoas->id_cliente == $cliente->id)
                  @if(!empty($cliente->image))
                        <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/".$cliente->id."/".$cliente->image) }}" alt="" style="width: 50px;height: 50px;">
                    @elseif($pessoas->sexo == 'Feminino')
                      <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/avatarf.png") }}" alt="" style="width: 50px;height: 50px;">
                      @elseif($pessoas->sexo == 'Masculino')
                      <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/avatar.jpg") }}" alt="" style="width: 50px;height: 50px;">
                  @elseif($pessoas->cpf == "")
                      <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/empresa.png") }}" alt="" style="width: 50px;height: 50px;">
                  @endif
                          @endif
                      @endforeach

              </td>
                 </td>
                   <td>{{ $cliente->nome }}</td>

                  <td>
                      <?php
                        foreach ($contatos as $contato){
                            if($cliente->id == $contato->id_cliente){
                                echo $contato->email;
                            }
                        }
                      ?>
                  </td>
                <td>
                    <?php
                    foreach ($contatos as $contato){
                        if($cliente->id == $contato->id_cliente){
                            echo $contato->celular;
                        }
                    }
                    ?>
                </td>

              <td style="text-align: center;"><a href="{{ route('clientes.edit', $cliente->id) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>
              <td style="text-align: center;"><a href="{{ route('clientes.remove', $cliente->id) }}"><i class="glyphicon glyphicon-trash"></i></a></td>
              <td style="text-align: center;"><a href="{{ route('clientes.show', $cliente->id) }}"><i class="glyphicon glyphicon-eye-open"></i></a></td>
          </tr>
      @endforeach
      </tbody>
  </table>
 </div>
  <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script>
      $(document).ready(function(){
          $('#minhaTabela').DataTable({
              "language": {
                  "lengthMenu": "Mostrando _MENU_ registros por página",
                  "zeroRecords": "Nada encontrado",
                  "info": "Mostrando página _PAGE_ de _PAGES_",
                  "infoEmpty": "Nenhum registro disponível",
                  "infoFiltered": "(filtrado de _MAX_ registros no total)"
              }
          });
      });
  </script>
   

@stop