@extends('adminlte::page')
<script type="text/javascript" src="<?php echo asset('js/estados.js')?>"></script>
@section('title', 'Sistema Viva Turismo')

@section('content')
    @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                            @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                        </div>
    @endif
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>

    </ol>
    <div class="container-fluid">
        <form action="{{ route('clientes.update', $cliente->id) }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            @csrf
            <div class="col-md-4" style="position: relative;float: left;">
                <div class="col-md-12">
                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile" style="overflow: hidden;">
                            @if(!empty($cliente->image))

                            <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/".$cliente->id."/".$cliente->image) }}" alt="" style="width: 100px;height: 100px;">

                            @elseif(!empty($pessoa->cpf))
                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/avatar.jpg") }}" alt="" style="width: 100px;height: 100px;">
                                @else
                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/empresa.png") }}" alt="" style="width: 100px;height: 100px;">
                            @endif

                            <h3 class="profile-username text-center"></h3>

                            <p class="text-muted text-center"></p>

                            <ul class="list-group list-group-unbordered" style="margin-top: 50px;">
                                <label for="exampleInputFile">Escolha a Imagem Para o Perfil</label>
                                <input type="file" name="image" id="uploadImage" onchange="PreviewImage();" style="width: 100%;border-radius: 5px;border: 1px solid gray;">

                            </ul>


                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h1 class="box-title">Formulario de Cadastro de Novos Clientes</h1>
                    </div>


                    <!-- /.box-header -->
                    <!-- form start -->

                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="col-md-6 form-group">
                            <label for="exampleInputName">Nome Completo</label>
                            <input type="text" name="nome" class="form-control" id="exampleInputName"  value="{{ $cliente->nome }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('name') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputDate">Data Nascimento</label>
                            <input type="text" name="dataNasc" class="form-control" id="exampleInputEmail1"  value="{{ date('d/m/Y',strtotime($cliente->dataNasc)) }}">
                            @if ($errors->has('Data Nascimento'))
                                <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('Data Nacimento') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="exampleInputEmail1">Compras</label>
                            <!--<input type="text" name="compras" class="form-control" id="exampleInputEmail1" placeholder="Compras" value="{{ $cliente->compras }}">-->
							<textarea name="compras" class="form-control" id="exampleInput"  value="">{{ $cliente->compras }}</textarea>
                            @if ($errors->has('compras'))
                                <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('compras') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="exampleInputDate">Escolha o Pais</label>
                            <!--<select name="pais" id="" class="form-control">
                                <option value="{{ $endereco->pais  }}">{{ $endereco->pais  }}</option>
                                <?php
                                $paises = "SELECT * FROM pais";
                                $paises = DB::select($paises);
                                ?>
                                @foreach($paises as $pais)
                                    <option value="{{ $pais->paisNome }}">{{ $pais->paisNome }}</option>
                                @endforeach
                            </select>-->
                            <input type="text" class="form-control" name="pais" value="{{ $endereco->pais  }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInput">Estado</label>
                            <input class="form-control" type="text" name="estado" value="{{ $endereco->estado }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInput">Cidade</label>
                            <input type="text" name="cidade" class="form-control" value="{{ $endereco->cidade }}"><!-- Só exibe depois que selecionar o estado -->
                        </div>
                        <div class="col-md-12" style="padding: 0px;">
                            <div class="col-md-6 form-group">
                                <label for="exampleInput">Rua</label>
                                <input type="text" name="rua" class="form-control" id="exampleInputEmail1" placeholder="Ex: Rua 04" value="{{ $endereco->rua }}">
                                @if ($errors->has('rua'))
                                    <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('rua') }}</strong>
                        </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="exampleInput">Numero</label>
                                <input type="text" name="numero" class="form-control" id="exampleInputEmail1" placeholder="Ex: 104" value="{{ $endereco->numero }}">
                                @if ($errors->has('numero'))
                                    <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('numero') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInput">Bairro</label>
                            <input type="text" name="bairro" class="form-control" id="exampleInputEmail1" placeholder="Ex: Centro" value="{{ $endereco->bairro }}">
                            @if ($errors->has('bairro'))
                                <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('bairro') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Cep</label>
                            <input type="text" name="cep" class="form-control" id="exampleInputEmail1" placeholder="Ex: 55880-000" value="{{ $endereco->cep }}">
                            @if ($errors->has('cep'))
                                <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('cep') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Complemento</label>
                            <input type="text" name="complemento" class="form-control" id="exampleInputEmail1" placeholder="Ex:Casa" value="{{ $endereco->complemento }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Fone Fixo:</label>
                            <input type="text" name="foneFixo" class="form-control" id="exampleInputEmail1" placeholder="Ex: 00-0000-0000" value="{{ $contato->foneFixo }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Fone Celular</label>
                            <input type="text" name="foneCelular" class="form-control" id="exampleInputEmail1" placeholder="Ex: 00-00000-0000" value="{{ $contato->foneCelular }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Fone Para Recado:</label>
                            <input type="text" name="foneRecado" class="form-control" id="exampleInputEmail1" placeholder="Ex: 00-0000-0000" value="{{ $contato->foneRecado }}">
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="exampleInputEmail1">Email:</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Ex: fulano@hotmail.com" value="{{ $contato->email }}">
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="exampleInputEmail1">Escolha o Tipo de pessoa</label>

                    </div>

                    @if(!empty($pessoa->cpf))
                    <div class="col-md-12 form-group" id="fisica" style="overflow: hidden;">
                        <h3>Dados Pessoa Física</h3>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Cpf:</label>
                            <input type="text" name="cpf" class="form-control" id="exampleInputEmail1" value="{{ $pessoa->cpf }}" onkeypress="mascara(this, '###.###.###-##')"  maxlength="14" />
                        </div>
						<div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">RG:</label>
                            <input type="text" name="rg" class="form-control" id="exampleInputEmail1" value="{{ $pessoa->rg }}" onkeypress="mascara(this, '##.####-#')"  maxlength="9" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Nacionalidade:</label>
                            <input type="text" name="nacionalidade" class="form-control" id="exampleInputEmail1" value="{{ $pessoa->nacionalidade }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Profissão:</label>
                            <input type="text" name="profissao" class="form-control" id="exampleInputEmail1" value="{{ $pessoa->profissao }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Sexo:</label>
                            <select name="sexo" id="" class="form-control">
                                <option value="{{ $pessoa->sexo }}"> {{ $pessoa->sexo }}</option>
                                <option value="Masculino">Masculino</option>
                                <option value="Feminino"> Feminino </option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Estado Civil:</label>
                            <select name="estadoCivil" id="" class="form-control">
                                <option value="{{ $pessoa->estadoCivil }}"> {{ $pessoa->estadoCivil }}</option>
                                <option value="Casado(a)">Casado(a)</option>
                                <option value="Solteiro(a)"> Solteiro(a)</option>
                                <option value="Divorciado(a)">Divorciado(a)</option>
                            </select>
                        </div>
                    </div>
                    @else
                    <div class="col-md-12 form-group" id="juridica">
                        <h3>Dados Pessoa Jurídica</h3>
                        <div class="col-md-12 form-group">
                            <label for="exampleInputEmail1">Cnpj:</label>
                            <input type="text" name="cnpj" class="form-control" id="exampleInputEmail1" value="{{ $juridica->cnpj }}" onkeypress="mascara(this, '##.###.###.####/##')" maxlength="18" />
                        </div>
                    </div>
                    @endif
                    <div class="box-footer">
                        <a href="{{ url()->previous() }}" class="btn btn-default">Voltar</a>
                        <button type="reset" class="btn btn-danger">Limpar</button>
                        <button type="submit" class="btn btn-success">Editar</button>
                    </div>
        </form>
    </div>
    </div>
    </div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script type="text/javascript">
        function PreviewImage() {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

            oFReader.onload = function (oFREvent) {
                //document.getElementById("uploadPreview").src = oFREvent.target.result;
                document.getElementById("uploadPreview").src = oFREvent.target.result;
            };
        };

    </script>

    <script type="text/javascript">

        $("#pessoa").on('change', function(e){
            var pessoa = $(this).val();
            $("#teste").html(pessoa);
            if (pessoa == 'Física'){
                $("#fisica").fadeIn(1000);
                $('#juridica').fadeOut(1000);
            }if (pessoa == 'Jurídica'){
                $("#fisica").fadeOut(1000);
                $('#juridica').fadeIn(1000);
            }

        });
    </script>
<script type="text/javascript">
        function mascara(t, mask){
            var i = t.value.length;
            var saida = mask.substring(1,0);
            var texto = mask.substring(i)
            if (texto.substring(0,1) != saida){
                t.value += texto.substring(0,1);
            }
        }
    </script>
@stop