@extends('adminlte::page')

@section('title', 'Sistema Viva Turismo')
<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
@section('content_header')
@stop

@section('content')
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        @for($i = 0; $i <= count(Request::segments()); $i++)
            <li>
                <a href="">{{Request::segment($i)}}</a>
                @if($i < count(Request::segments()) & $i > 0)
                    {!!'<i class="fa fa-angle-right"></i>'!!}
                @endif
            </li>
        @endfor
    </ul>
    <div class="col-md-12" style="background: darkred;margin: 10px;text-align: center;border-radius: 5px;color: white;">
        <h2>Tem Centeza que deseja deletar esse Cliente?</h2>
    </div>
    <div class="container-fluid">
        <!-- Main content -->
        <div class="col-md-3">

            <div class="row">
                <div class="col-md-12">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            @if(!empty($clientes->image))

                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/".$clientes->id."/".$clientes->image) }}" alt="" style="width: 100px;height: 100px;">

                            @elseif(!empty($pessoa->cpf))
                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/avatar.jpg") }}" alt="" style="width: 100px;height: 100px;">
                            @else
                                <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="{{ url("uploads/avatar/empresa.png") }}" alt="" style="width: 100px;height: 100px;">
                            @endif

                            <h3 class="profile-username text-center">{{ $clientes['nome'] }}</h3>

                            <p class="text-muted text-center">Profissão: {{ $pessoa['profissao'] }}</p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Cpf/Cnpj</b> <a class="pull-right">{{ $pessoa['cpf'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Nacionalidade</b> <a class="pull-right">{{ $pessoa['nacionalidade'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Estado Civil</b> <a class="pull-right">{{ $pessoa['estadoCivil'] }}</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- About Me Box -->

                    <!-- /.box -->
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                         <form method="post" action="{{ route('clientes.destroy', $clientes->id)  }}">
                            {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        {{ csrf_field() }}
                        <!-- Post -->
                        <div class="post">
                            <div class="user-block">
                                @if(!empty($clientes->image))

                                    <img class="img-circle img-bordered-sm"  src="{{ url("uploads/avatar/".$clientes->id."/".$clientes->image) }}" alt="" >

                                @elseif(!empty($pessoa->cpf))
                                    <img class="img-circle img-bordered-sm"  src="{{ url("uploads/avatar/avatar.jpg") }}" alt="" >
                                @else
                                    <img class="img-circle img-bordered-sm"  src="{{ url("uploads/avatar/empresa.png") }}" alt="" >
                                @endif
                                <span class="username">
                          <a href="#">{{ $clientes->nome }}.</a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                                <span class="description">Cliene cadastrado em {{ date( 'd/m/Y  H:m:s' , strtotime($clientes->created_at)) }}</span>
                            </div>
                            <!-- /.user-block -->
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Data de Nascimento: <i style="color: royalblue">{{ $clientes->dataNasc }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Compras: <i style="color: royalblue">{{ $clientes->compras }}</i>
                            </p>
                            <h3 style="text-align: center;">Contatos</h3>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Celular: <i style="color: royalblue">{{ $contato->foneCelular }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Telefone Fixo: <i style="color: royalblue">{{ $contato->foneFixo }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Telefone Para Recado: <i style="color: royalblue">{{ $contato->foneRecado }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Email: <i style="color: royalblue">{{ $contato->email }}</i>
                            </p>
                            <h3 style="text-align: center;">Endereço</h3>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                País: <i style="color: royalblue">{{ $endereco->pais }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Estado: <i style="color: royalblue">{{ $endereco->estado }}</i>
                            </p>
                            <p style="font-size: 18px;border-bottom: 1px solid #edecec;">
                                Cidade: <i style="color: royalblue">{{ $endereco->cidade }}</i>
                            </p>
                        </div>
                         <button type="submit" class="btn btn-danger">Remover</button>
                          <a href="{{ route('clientes.index') }}" class="btn btn-default">Voltar</a>
                         </form>
                    </div>
@stop
