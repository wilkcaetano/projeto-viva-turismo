@extends('adminlte::page')
<script type="text/javascript" src="<?php echo asset('js/estados.js')?>"></script>

@section('title', 'Sistema Viva Turismo')
@section('content')

    @if(Session::has('message'))
        <div id="msg" class="alert alert-success" style="text-align: center;">
            <p>{{ Session::get('message') }}</p>
        </div>
    @endif  
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{route('home')}}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>

    </ol>
    <div class="container-fluid">
    <form action="{{ action('ClienteController@store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="col-md-4" style="position: relative;float: left;">
            <div class="col-md-12">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile" style="overflow: hidden;">
                        <img class="profile-user-img img-responsive img-circle" id="uploadPreview" src="https://thumbs.dreamstime.com/b/moderno-masculino-do-avatar-do-%C3%ADcone-do-perfil-do-homem-de-neg%C3%B3cio-69046013.jpg" alt="" style="width: 150px;height: 150px;">

                        <h3 class="profile-username text-center"></h3>

                        <p class="text-muted text-center"></p>

                        <ul class="list-group list-group-unbordered" style="margin-top: 50px;">
                        <ul class="list-group list-group-unbordered" style="margin-top: 50px;">
                            <label for="exampleInputFile">Escolha a Imagem Para o Perfil</label>
                            <input type="file" name="image" id="uploadImage" onchange="PreviewImage();" style="width: 100%;border-radius: 5px;border: 1px solid gray;">

                        </ul>

                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h1 class="box-title">Formulario de Cadastro de Novos Clientes</h1>
                </div>


                <!-- /.box-header -->
                <!-- form start -->

                {{ csrf_field() }}
                <div class="box-body">
                    <div class="col-md-6 form-group">
                        <label for="exampleInputName">Nome Completo</label>
                        <input type="text" name="nome" class="form-control" id="exampleInputName" value="{{old('nome')}}" placeholder="Ex: José da Silva">
                        @if ($errors->has('name'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="exampleInputDate">Data Nascimento</label>
                        <input type="date" name="dataNasc" class="form-control" id="exampleInputDate" placeholder="Ex: 00/00/0000" value="{{old('dataNasc')}}" maxlength="10" required>
                        @if ($errors->has('Data Nascimento'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('Data Nacimento') }}</strong>
                        </span>
                        @endif
                    </div>
                    <!-- -->
                    <div class="col-md-12 form-group">
                        <label for="exampleInputEmail1">Escolha o Tipo de pessoa</label>
                        <select name="pessoa" id="pessoa" class="form-control">
                            <option value=""></option>
                            <option value="Física">Física</option>
                            <option value="Jurídica">Jurídica</option>
                        </select>
                    </div>
                    <div class="col-md-12 form-group" id="fisica" style="overflow: hidden;">
                        <h3>Dados Pessoa Física</h3>
                        <div class="col-md-6 form-group">
                            <label for="">Cpf:</label>
                            <input type="text" name="cpf" class="form-control" id="exampleInput" onkeypress="mascara(this, '###.###.###-##')" value="{{old('cpf')}}" maxlength="14">
							 @if ($errors->has('cpf'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('cpf') }}</strong>
                        </span>
                        @endif
                        </div>
						<div class="col-md-6 form-group">
                            <label for="">RG:</label>
                            <input type="text" name="rg" class="form-control" id="exampleInput" onkeypress="mascara(this, '##.####-#')" value="{{old('rg')}}" maxlength="9">
							@if ($errors->has('rg'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('rg') }}</strong>
							@endif
                        </span>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="">Nacionalidade:</label>
                            <input type="text" name="nacionalidade" class="form-control" id="exampleInputEmail1"  value="{{old('nacionalidade')}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="">Profissão:</label>
                            <input type="text" name="profissao" class="form-control" id="exampleInputEmail1"  value="{{old('profissao')}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="">Sexo:</label>
                            <select name="sexo" id="" class="form-control">
                                <option value=""></option>
                                <option value="Masculino">Masculino</option>
                                <option value="Feminino"> Feminino </option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Estado Civil:</label>
                            <select name="estadoCivil" id="" class="form-control">
                                <option value=""></option>
                                <option value="Casado(a)">Casado(a)</option>
                                <option value="Solteiro(a)"> Solteiro(a)</option>
                                <option value="Divorciado(a)">Divorciado(a)</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 form-group" id="juridica">
                        <h3>Dados Pessoa Jurídica</h3>
                        <div class="col-md-12 form-group">
                            <label for="exampleInputEmail1">Cnpj:</label>
                            <input type="text" name="cnpj" class="form-control" id="exampleInput" placeholder="Ex: 00-0000-0000" onkeypress="mascara(this, '##.###.###.####/##')" maxlength="18"/>
                        </div>
                    </div>
                    <!-- -->

                    <div class="col-md-12 form-group">
                        <label for="exampleInputDate">Escolha o Pais</label>
                        <!--<select name="pais" id="" class="form-control">
                            <option value=""></option>
                            <?php
                            $paises = "SELECT * FROM pais";
                            $paises = DB::select($paises);
                            ?>
                            @foreach($paises as $pais)
                                <option value="{{ $pais->paisNome }}">{{ $pais->paisNome }}</option>
                            @endforeach
                        </select>-->
                        <input type="text"name="pais" class="form-control" placeholder="Ex: Brasil" value="{{old('pais')}}" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="exampleInputEmail1">Estado</label>
                        <!--<select name="estado" id="uf" class="form-control">
                            <option value="">-- Escolha o Estado --</option>
                        </select>-->
                        <input type="text"name="estado" class="form-control" placeholder="Ex: Paraiba" value="{{old('estado')}}" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="exampleInputEmail1">Cidade</label>
                       <!-- <select name="cidade" id="cidade" class="form-control" style="display:none;">

                        </select>--> <!-- Só exibe depois que selecionar o estado -->
                        <input type="text"name="cidade" class="form-control" placeholder="Ex: João Pessoa" value="{{old('cidade')}}" required />
                    </div>
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Rua</label>
                            <input type="text" name="rua" class="form-control" id="exampleInputEmail1" placeholder="Ex: Rua 04" value="{{old('rua')}}" required />
                            @if ($errors->has('rua'))
                                <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('rua') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Numero</label>
                            <input type="text" name="numero" class="form-control" id="" placeholder="Ex: 104" value="{{old('numero')}}">
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="exampleInputEmail1">Bairro</label>
                        <input type="text" name="bairro" class="form-control" id="" placeholder="Ex: Centro" value="{{old('bairro')}}" required />

                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Cep</label>
                        <input type="text" name="cep" class="form-control" id="" placeholder="Ex: 55880-000" onkeypress="mascara(this, '#####-###')" value="{{old('cep')}}" maxlength="9" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Complemento</label>
                        <input type="text" name="complemento" class="form-control" id="" placeholder="Ex:Casa" value="{{old('complemento')}}">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Fone Fixo:</label>
                        <input type="text" name="foneFixo" class="form-control" id="" onkeypress="mascara(this, '## ####-####')" placeholder="Ex: 00-0000-0000" value="{{old('foneFixo')}}">
                        @if ($errors->has('foneFixo'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('foneFixo') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="exampleInput">Celular:</label>
                        <input type="text" name="celular" class="form-control" id="" placeholder="Ex: 00-0000-0000" onkeypress="mascara(this, '## ####-####')" value="{{old('celular')}}" c>
                        @if ($errors->has('celular'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('celular') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="exampleInput">Fone Para Recado:</label>
                        <input type="text" name="foneRecado" class="form-control" id="" placeholder="Ex: 00-0000-0000" onkeypress="mascara(this, '## ####-####')" value="{{old('foneRecado')}}">
                        @if ($errors->has('foneRecado'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('foneRecado') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="">Email:</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Ex: fulano@hotmail.com" value="{{old('email')}}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label for="exampleInputEmail1">Compras</label>
                    <textarea name="compras" class="form-control" style="height: 200px;"></textarea>
                    @if ($errors->has('compras'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('compras') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="box-footer">
                    <button type="reset" class="btn btn-danger">Limpar</button>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
    </form>
    </div>
    </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script type="text/javascript">
        function PreviewImage() {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

            oFReader.onload = function (oFREvent) {
                //document.getElementById("uploadPreview").src = oFREvent.target.result;
                document.getElementById("uploadPreview").src = oFREvent.target.result;
            };
        };

    </script>
        <script type="text/javascript">
            $(document).ready(function() {
                //verificar
                $.ajax({
                    url: 'https://gist.githubusercontent.com/letanure/3012978/raw/36fc21d9e2fc45c078e0e0e07cce3c81965db8f9/estados-cidades.json'
                    ,type:'GET'
                    ,dataType: 'json'
                    ,cache: true
                    ,success: function(json){
                        //Fala que o json a Var global json (window.json) é json
                        window.json = json;

                        var seletorUf = $("#uf");
                        for(i in json.estados){
                            var estado = json.estados[i];
                            $("<option />", {value: estado.sigla, text: estado.nome}).appendTo(seletorUf);
                        }
                    }
                    ,error: function(json){
                        //console.log(json);
                    }
                });

                $( "#uf" ).bind( "change", function() {
                    var ufSelecionado = $( this ).val();

                    var selectCidades = $("#cidade");
                    selectCidades.empty();
                    selectCidades.show();

                    //Percorre todo o Loop de estados
                    for(i in json.estados){
                        var estado = json.estados[i];

                        //Caso a sigla seja a mesma selecionada
                        if(estado.sigla == ufSelecionado){
                            for(x in estado.cidades){
                                var cidade = estado.cidades[x];
                                $("<option />", {value: cidade, text: cidade}).appendTo(selectCidades);
                            }

                            //Break loop (Improve performace?)
                            return false;
                        }
                    }
                });
            });
    </script>
    <script type="text/javascript">
        $(function(){
            $("#juridica").css("display","none");
            $("#fisica").css("display","none");
        });

        $("#pessoa").on('change', function(e){
            var pessoa = $(this).val();
            $("#teste").html(pessoa);
            if (pessoa == 'Física'){
                $("#fisica").fadeIn(1000);
                $('#juridica').fadeOut(1000);
            }if (pessoa == 'Jurídica'){
                $("#fisica").fadeOut(1000);
                $('#juridica').fadeIn(1000);
            }if(pessoa == ""){
                $("#fisica").fadeOut(1000);
                $('#juridica').fadeOut(1000);
            }

        });
    </script>
	<script type="text/javascript">
	 function mascara(t, mask){
		 var i = t.value.length;
		 var saida = mask.substring(1,0);
		 var texto = mask.substring(i)
		 if (texto.substring(0,1) != saida){
		 t.value += texto.substring(0,1);
		 }
	 }
</script>

@stop