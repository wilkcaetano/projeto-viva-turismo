<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PessoaJuridica extends Model
{
    protected $fillable = [
        'id_cliente', 'cnpj',
    ];

    protected $table = 'pessoajuridica';
}
