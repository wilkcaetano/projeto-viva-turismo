<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
        'id_cliente', 'cep', 'rua', 'numero', 'cidade', 'bairro', 'complemento',
        'estado', 'pais',
    ];

    protected $table = 'endereco';
}
