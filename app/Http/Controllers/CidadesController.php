<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;

class CidadesController extends Controller
{
    private $estadoModel;
    public function __construct(Estado $estado)
    {
        $this->estadoModel = $estado;
    }
    public function index()
    {
        $estados = $this->estadoModel->list('estado', 'id');
        return view('clientes.cadastrar', compact('estados'));
    }
    public function getCidades($cod_estados)
    {
        $cidades = DB::table('cidades')
            ->where('estados_cod_estados','=', $cod_estados)
            ->orderBy('name','asc')
            ->get();
        return Response::json($cidades);
    }
}
