<?php

namespace App\Http\Controllers;

use App\Contato;
use App\Endereco;
use App\Documnetos;
use App\Http\Requests\ContatoEnviarRequest;
use App\PessoaFisica;
use App\PessoaJuridica;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Cliente;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $clientes = Cliente::all();
        $contatos = Contato::all();
        $pessoa = PessoaFisica::all();
        $juridica = PessoaJuridica::all();
        return view('clientes.index', compact('clientes', 'contatos', 'pessoa', 'juridica'));    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.cadastrar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */



    public function store(Request $request)
    {
        $cliente = new Cliente();
        $cliente->nome = $request->post('nome');
        $cliente->dataNasc = $request->post('dataNasc');
        $cliente->compras = $request->post('compras');
        $cliente->image = $request->post('image');
        $cliente->save();
        $id = $cliente->id;
        $contato = new Contato();
        $contato->id_cliente = $id;
        $contato->foneFixo = $request->post('foneFixo');
        $contato->celular = $request->post('celular');
        $contato->foneRecado = $request->post('foneRecado');
        $contato->email = $request->post('email');
        $contato->save();
        $endereco = new Endereco();
        $endereco->id_cliente = $id;
        $endereco->cep = $request->post('cep');
        $endereco->rua = $request->post('rua');
        $endereco->numero = $request->post('numero');
        $endereco->cidade = $request->post('cidade');
        $endereco->bairro = $request->post('bairro');
        $endereco->complemento = $request->post('complemento');
        $endereco->estado = $request->post('estado');
        $endereco->pais = $request->post('pais');
        $endereco->save();
        $pessoaFisica = new PessoaFisica();
        $pessoaFisica->id_cliente = $id;
        $pessoaFisica->cpf = $request->post('cpf');
        $pessoaFisica->rg = $request->post('rg');
        $pessoaFisica->nacionalidade = $request->post('nacionalidade');
        $pessoaFisica->profissao = $request->post('profissao');
        $pessoaFisica->sexo = $request->post('sexo');
        $pessoaFisica->estadoCivil = $request->post('estadoCivil');
        $pessoaFisica->save();
        $pessoaJuridica = new PessoaJuridica();
        $pessoaJuridica->id_cliente = $id;
        $pessoaJuridica->cnpj = $request->post('cnpj');
        $pessoaJuridica->save();
        /*return redirect()
            ->back()
            ->with('message','Cliente Cadastrado com Sucesso!');*/
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $nome = $cliente->nome;
            $primeiroNome = explode(" ", $nome);
            $name = "foto_perfil_".$primeiroNome[0];
            $extension = $request->image->extension();
            $nameFile = "{$name}.{$extension}";
            $cliente->image = $nameFile;
            $pasta = $request->image->storeAs('uploads/avatar', $id);
            $upload = $request->image->move($pasta, $nameFile);
            if(!$upload)
                return redirect()
                    ->back()
                    ->with('error', 'Falha ao enviar a Imagem');
            $cliente->save();
        }
        return redirect()->back()->with('message','Cliente Cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientes = Cliente::find($id);
        $ident = $clientes['id'];
        $pessoa = PessoaFisica::where('id_cliente', '=', $ident)->firstOrFail();
        $juridicap = PessoaJuridica::where('id_cliente', '=', $ident)->firstOrFail();
        $contato = Contato::where('id_cliente', '=', $ident)->firstOrFail();
        $endereco = Endereco::where('id_cliente', '=', $ident)->firstOrFail();
        $documentos = Documnetos::all();
        return view('clientes.show', compact('clientes', 'pessoa', 'contato', 'endereco', 'juridicap','documentos'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        $ident = $cliente['id'];
        $pessoa = PessoaFisica::where('id_cliente','=',$ident)->firstOrFail();
        $contato = Contato::where('id_cliente','=',$ident)->firstOrFail();
        $juridica = PessoaJuridica::where('id_cliente','=',$ident)->firstOrFail();
        $endereco = Endereco::where('id_cliente','=',$ident)->firstOrFail();
        return view('clientes.edit', compact('cliente', 'pessoa', 'contato', 'endereco','juridica'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        $ident = $cliente['id'];
        $pessoa = PessoaFisica::where('id_cliente','=',$ident)->firstOrFail();
        $contato = Contato::where('id_cliente','=',$ident)->firstOrFail();
        $endereco = Endereco::where('id_cliente','=',$ident)->firstOrFail();
        $dados = $request->all();
        $cliente->update($dados);
        $pessoa->update($dados);
        $contato->update($dados);
        $endereco->update($dados);
        if(empty($cliente->image)) {
            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $nome = $cliente->nome;
                $primeiroNome = explode(" ", $nome);
                $name = "foto_perfil_" . $primeiroNome[0];
                $extension = $request->image->extension();
                $nameFile = "{$name}.{$extension}";
                $cliente->image = $nameFile;
                $pasta = $request->image->storeAs('uploads/avatar', $id);
                $upload = $request->image->move($pasta, $nameFile);
                if (!$upload)
                    return redirect()
                        ->back()
                        ->with('error', 'Falha ao enviar a Imagem');
            }
            $cliente->update($dados);
        }else{
            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $nome = $cliente->nome;
                $primeiroNome = explode(" ", $nome);
                $name = "foto_perfil_" . $primeiroNome[0];
                $extension = $request->image->extension();
                $nameFile = "{$name}.{$extension}";
                $cliente->image = $nameFile;
                $pasta = $request->image->storeAs('uploads/avatar', $id);
                $upload = $request->image->move($pasta, $nameFile);
                if (!$upload)
                    return redirect()
                        ->back()
                        ->with('error', 'Falha ao enviar a Imagem');
            }
        }
        return redirect()->route('clientes.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function register()
    {
        return view('clientes.register');
    }

    public function destroy($id)
    {
        Cliente::find($id)->delete();
        return redirect()->route('clientes.index');
    }


    public function remove($id)
    {
        $clientes = Cliente::find($id);
        $ident = $clientes['id'];
        $pessoa = PessoaFisica::where('id_cliente','=',$ident)->firstOrFail();
        $contato = Contato::where('id_cliente','=',$ident)->firstOrFail();
        $endereco = Endereco::where('id_cliente','=',$ident)->firstOrFail();
        return view('clientes.remove', compact('clientes', 'pessoa', 'contato', 'endereco'));
    }
	
	public function documentos (Request $request, $id)
    {

        $documento = new Documnetos();
        $documento->id_cliente = $request->post('id_cliente');
        $documento->nome = $request->post('nome');
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $nome = $documento->nome;
            $primeiroNome =  preg_replace('/[ -]+/' , '-' , $nome);
            $name = "documento-" . $primeiroNome;
            $extension = $request->image->extension();
            $nameFile = "{$name}.{$extension}";
            $documento->image = $nameFile;
            $pasta = $request->image->storeAs('uploads/avatar', $id);
            $upload = $request->image->move($pasta, $nameFile);
            if (!$upload)
                return redirect()
                    ->back()
                    ->with('error', 'Falha ao enviar a Imagem');

        }
        $documento->save();
        return redirect()
            ->back()
            ->with('message', 'Documento Cadastrado Com Sucesso');

    }
	
	  public function documentosdelete($id)
    {
       Documnetos::find($id)->delete();
        return redirect()->back()->with('message', 'Documento do Cliente Deletado');
    }

	
	
	public function download($id)
    {
        $clientes = Cliente::find($id);
        $ident = $clientes->id;
        $contato = Contato::where('id_cliente','=',$ident)->firstOrFail();
        $endereco = Endereco::where('id_cliente','=',$ident)->firstOrFail();
        $juridica = PessoaJuridica::where('id_cliente','=',$ident)->firstOrFail();
        $pessoa = PessoaFisica::where('id_cliente','=',$ident)->firstOrFail();
		$data = date( 'd/m/Y' , strtotime($clientes->dataNasc));
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML("
          <style>
            .info{
                margin: 5px;
              
            }
            .info span{
                mso-font-width: 600px;
               
            }
          </style>
            <body style='font-family: arial;'>
                <h1 style='text-align: center; font-family: Calibri Light;margin-top: -20px;'>$clientes->nome</h1>
                <hr>
                <div class='container'>
                    <div class='col-md-4' style='position: relative; float: left;width: 25%; padding: 10px;'>
                        <div class='image' style='width: 150px; height: 200px;  overflow: hidden;background: #edecec;'>
                            <img src='uploads/avatar/$clientes->id/$clientes->image' alt='' style='width: 100%; height: 100%;'>
                        </div>
                    </div>
                    <div class='col-md-8' style='position: relative; float: right; width: 75%;padding: 10px;'>
                        <h3>Dados do Cliente</h3>
                        <hr>
                        <div class='tabela'>
                            <div class='info'>
                               <span>Nome Completo: </span>
                               $clientes->nome                  
                            </div> 
                            <div class='info'>
                               <span>Data de Nascimento: </span>
							     $data             
                            </div> 
                             <div class='info'>
                               <span>Compras Relacionadas: </span>
                               $clientes->compras                  
                            </div> 
                            <h3>Contatos do Cliente</h3>
                             <hr>
                             <div class='info'>
                               <span>Fone Fixo: </span>
                               $contato->foneFixo                  
                            </div> 
                            <div class='info'>
                               <span>Fone Celular: </span>
                               $contato->foneCelular                  
                            </div> 
                            <div class='info'>
                               <span>Fone Recado: </span>
                               $contato->foneRecado                  
                            </div> 
                            <div class='info'>
                               <span>Email: </span>
                               $contato->email                  
                            </div> 
                            <h3>Endereço do Cliente</h3>
                            <hr>
                            <div class='info'>
                               <span>Pais: </span>
                               $endereco->pais                  
                            </div> 
                            <div class='info'>
                               <span>Estado: </span>
                               $endereco->estado                  
                            </div> 
                            <div class='info'>
                               <span>Cidade: </span>
                               $endereco->cidade                  
                            </div> 
                            <div class='info'>
                               <span>Rua: </span>
                               $endereco->rua                  
                            </div> 
                            <div class='info'>
                               <span>Bairro: </span>
                               $endereco->bairro                  
                            </div> 
                            <div class='info'>
                               <span>Complemento: </span>
                               $endereco->complemento                  
                            </div> 
                            <div class='info'>
                               <span>Número: </span>
                               $endereco->numero                  
                            </div> 
                            <div class='info'>
                               <span>CEP: </span>
                               $endereco->cep                  
                            </div> 
                            <h3>Pessoa Física</h3>
                            <hr>
                            <div class='info'>
                               <span>CPF: </span>
                               $pessoa->cpf                  
                            </div>
                            <div class='info'>
                               <span>Nacionalidade: </span>
                               $pessoa->nacionalidade                  
                            </div> 
                            <div class='info'>
                               <span>Profissão: </span>
                               $pessoa->profissao                  
                            </div> 
                            <div class='info'>
                               <span>Estado Civil: </span>
                               $pessoa->estadoCivil                  
                            </div> 
                            <div class='info'>
                               <span>Sexo: </span>
                               $pessoa->sexo                  
                            </div> 
                             <h3>Pessoa Jurídica</h3>
                            <hr>
                            <div class='info'>
                               <span>CNPJ: </span>
                               $juridica->cnpj                  
                            </div> 
                        </div>
                    </div>
                </div>
            </body>
            </html>
    
        ");
        $titulo = $clientes->nome;
        $titulo_novo = strtolower( preg_replace("[^a-zA-Z0-9-]", "-",
            strtr(utf8_decode(trim($titulo)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),
                "aaaaeeiooouuncAAAAEEIOOOUUNC-")) );
        return $pdf->stream($titulo_novo.'.pdf');
    }
}
