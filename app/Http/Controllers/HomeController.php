<?php

namespace App\Http\Controllers;

use App\PessoaFisica;
use Illuminate\Http\Request;
use App\Cliente;
use App\PessoaJuridica;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();
        $pessoa = PessoaFisica::all();
		$juridica = PessoaJuridica::all();
        return view('home', compact('clientes','pessoa', 'juridica'));
		
    }
	
	
}
