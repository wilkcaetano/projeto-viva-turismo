<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContatoEnviarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nome" => "required",
            "compras" => "required",
            "pais" => "required",
            "estado" => "required",
            "cidade" => "required",
            "rua" => "required",
            "numero" => "required | numeric",
            "cep" => "required | numeric",
            "foneCelular" => "required | numeric",
            "email" => "required ",
        ];
    }
}
