<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $fillable = [
        'id_cliente', 'foneFixo', 'celular', 'foneRecado', 'email'
    ];

    protected $table = 'contato';
}
