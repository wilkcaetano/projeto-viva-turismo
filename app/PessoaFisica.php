<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PessoaFisica extends Model
{
    protected $fillable = [
        'id_cliente', 'cpf', 'rg', 'nacionalidade', 'profissao', 'estadoCivil',
    ];

    protected $table = 'pessoafisica';
}
