<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documnetos extends Model
{
    protected $fillable = [
        'id_cliente', 'documentos',
    ];

    protected $table = 'documentos';
}
