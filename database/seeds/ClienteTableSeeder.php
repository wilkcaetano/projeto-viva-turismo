<?php

use Illuminate\Database\Seeder;
use App\Cliente;
use App\Endereco;
use App\Contato;
use App\PessoaFisica;
use App\PessoaJuridica;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::create([
            'id' => 3,
            'nome' => 'Nicolas dos Santos França',
            'dataNasc' => '10/05/2012',
            'compras' => 'Pacote completo para Rio de Janeiro',
        ]);
        Endereco::create([
            'id_cliente' => 3,
            'cep' => '55880000',
            'rua' => '04',
            'numero' => '50',
            'cidade' => 'Ferreiros',
            'bairro' => 'Loteamento Pará',
            'complemento' => 'Casa',
            'estado' => 'Pernambuco',
            'pais' => 'Brasil',
        ]);
        Contato::create([
            'id_cliente' => 3,
            'foneFixo' => '81 3631-1077',
            'foneCelular' => '81 993949202',
            'foneRecado' => '00 0000-000',
            'email' => 'fulano@hotmail.com',
        ]);
        PessoaJuridica::create([
            'id_cliente' => 3,
            'cnpj' => '',
        ]);
        PessoaFisica::create([
            'id_cliente' => 3,
            'cpf' => '06681835430',
            'nacionalidade' => 'Brasileira',
            'profissao' => 'Programador',
            'estadoCivil' => 'Solteiro',
        ]);


    }
}
